﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seldat {
    public class Category {
        [Key]
        public int CategoryId { get; set; }
        [Required]
        public string CatogoryName { get; set; }
        public virtual ICollection<Clothing> Clothes { get; set; }
        public virtual ICollection<Type> Types { get; set; }

    }
}
