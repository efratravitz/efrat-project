﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seldat {
   public class Clothing {
     
        [Key]
        public int ClothId { get; set; }
        
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
      
        [ForeignKey("Type")]

        public int TypeId { get; set; }
       
        [ForeignKey("Company")]

        public int ComanyId { get; set; }
        [Required]
        public decimal Price { get; set; }
        public float? DisCount { get; set; }
        [Required]
        public string Image { get; set; }

       public virtual Category Category { get; set; }
      public virtual Company Company { get; set; }
    public virtual Type Type { get; set; }
    }
}
