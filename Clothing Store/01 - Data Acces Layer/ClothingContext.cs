﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seldat {
  public  class ClothingContext:DbContext {
        public ClothingContext() :base("clothingShop") {

            Database.SetInitializer<ClothingContext>(new CreateDatabaseIfNotExists<ClothingContext>());

        }
        public DbSet<Clothing> clothes { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Admin> Admin { get; set; }
    }
}
