﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seldat {
 public   class Type {
        [Key]
        public int TypeId { get; set; }
        [Required]
        public string TypeName { get; set; }
        public virtual ICollection<Clothing> Clothes { get; set; }
        public virtual ICollection<Category> Categories { get; set; }

    }
}
