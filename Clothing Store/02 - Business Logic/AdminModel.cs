﻿using System.ComponentModel.DataAnnotations;

namespace Seldat {
    public class AdminModel {
        public int id { get; set; }     
        [RegularExpression("^\\w{3,}$", ErrorMessage = "שם חייב להכיל לפחות 3 תווים")]
        public string name { get; set; }
        [RegularExpression("^[0-9]{4,8}$", ErrorMessage = "סיסמא לא חוקית. יש להקיש 4-8 תווים.")]

        public string password { get; set; }
        
    }
}
