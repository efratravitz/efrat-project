﻿using System.ComponentModel.DataAnnotations;

namespace Seldat {
    public class CategoryModel {
        public int id { get; set; }
        [Required(ErrorMessage = "Missing Name Category")]
        public string name { get; set; }
    }
}
