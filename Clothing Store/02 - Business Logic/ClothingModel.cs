﻿using System.ComponentModel.DataAnnotations;

namespace Seldat {
    public class ClothingModel {

        public int id { get; set; }
        public CategoryModel category { get; set; }
        public TypeModel type { get; set; }
        public CompanyModel company { get; set; }

        [Required(ErrorMessage = "Missing Price")]
        [Range(0, int.MaxValue, ErrorMessage = "Price Cant Be Negative")]

        public decimal price { get; set; }
        public float? discount { get; set; }
        public string image { get; set; }
    }
}
