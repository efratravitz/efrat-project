﻿using System.ComponentModel.DataAnnotations;

namespace Seldat {
    public class CompanyModel {
        public int id { get; set; }

        [Required(ErrorMessage = "Missing CompanyName")]

        public string name { get; set; }
    }
}
