﻿using System.ComponentModel.DataAnnotations;

namespace Seldat {
    public class TypeModel {
        public int id { get; set; }

        [Required(ErrorMessage = "Missing TypeName")]
        public string name { get; set; }

    }
}
