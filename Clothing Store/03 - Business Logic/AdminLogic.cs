﻿using System.Linq;

namespace Seldat {
    public class AdminLogic : BaseLogic {
        //Check Admin

        public bool CheckAdmin(AdminModel admin) {
            return DB.Admins.Count(a => a.AdminName == admin.name && a.AdminPassword == admin.password) > 0;
        }
    }
}
