﻿using System;

namespace Seldat {
    public class BaseLogic : IDisposable {
        protected Clothing_StoreEntities DB = new Clothing_StoreEntities();

        public void Dispose() {
            DB.Dispose();
        }
    }
}
