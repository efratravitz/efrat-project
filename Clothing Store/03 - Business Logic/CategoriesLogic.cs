﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class CategoriesLogic : BaseLogic {

        public List<CategoryModel> GetAllCategories() {
            return DB.Categories.Select(j => new CategoryModel {
                id = j.CategoryId,
                name = j.CatogoryName
            }).ToList();
        }

 
    }
}
