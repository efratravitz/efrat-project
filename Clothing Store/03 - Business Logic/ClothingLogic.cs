﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class ClothingLogic : BaseLogic {


        public List<ClothingModel> GetAllClothes() {
            return DB.Clothes.Select(j => new ClothingModel {
                id = j.ClothId,
                category = new CategoryModel() { id = j.Category.CategoryId, name = j.Category.CatogoryName },
                company = new CompanyModel() { id = j.Company.CompanyId, name = j.Company.ComanyName },
                type = new TypeModel { id = j.Type.TypeId, name = j.Type.TypeName },
                price = j.Price,
                discount = j.DisCount,
                image = j.Image
            }).ToList();
        }
        public List<ClothingModel> GetClothesByCategory(int categoryId) {
            System.Console.WriteLine(categoryId);
            return DB.Clothes.Include("Categories").Include("Types").Include("Companies").
                Where(c => c.CategoryId == categoryId)
                  .Select(c => new ClothingModel {
                      id = c.ClothId,
                      category = new CategoryModel { id = c.Category.CategoryId, name = c.Category.CatogoryName },
                      company = new CompanyModel { id = c.Company.CompanyId, name = c.Company.ComanyName },
                      type = new TypeModel { id = c.Type.TypeId, name = c.Type.TypeName },
                      price = c.Price,
                      discount = c.DisCount,
                      image = c.Image
                  }).ToList();
        }


        public void SaveImage(int clothingID, string image) {
            Cloth clothing = DB.Clothes.FirstOrDefault(c => c.ClothId == clothingID);
            clothing.Image = image;
            DB.SaveChanges();
        }


        public ClothingModel GetOneCloth(int id) {
            return DB.Clothes.Where(j => j.ClothId == id).Select(j => new ClothingModel {
                id = j.ClothId,
                category = new CategoryModel() { id = j.Category.CategoryId, name = j.Category.CatogoryName },
                company = new CompanyModel() { id = j.Company.CompanyId, name = j.Company.ComanyName },
                type = new TypeModel { id = j.Type.TypeId, name = j.Type.TypeName },
                price = j.Price,
                discount = j.DisCount,
                image = j.Image
            }).FirstOrDefault();
        }

        public ClothingModel AddCloth(ClothingModel model) {
            Cloth cloth = new Cloth {
                CategoryId = model.category.id,
                ComanyId = model.company.id,
                TypeId = model.type.id,
                Price = model.price,
                DisCount = model.discount,
                Image = model.image,
            };
            DB.Clothes.Add(cloth);
            DB.SaveChanges();
            model.id = cloth.ClothId;
            model.image = cloth.Image;
            return model;
        }
 

        public ClothingModel UpdateFullClothes(ClothingModel model) {
            Cloth cloth = new Cloth { ClothId = model.id };
            DB.Clothes.Attach(cloth);
            cloth.Category = DB.Categories.FirstOrDefault(c => c.CategoryId == model.category.id);
            cloth.Type = DB.Types.FirstOrDefault(c => c.TypeId == model.type.id);
            cloth.Company = DB.Companies.FirstOrDefault(c => c.CompanyId == model.company.id);
            cloth.Price = model.price;
            cloth.DisCount = model.discount;
            if (model.image != null)
                cloth.Image = model.image;
            DB.SaveChanges();
            return model;
        }
        public ClothingModel UpdatePartialCloth(ClothingModel model) {
            Cloth cloth = DB.Clothes.FirstOrDefault(c => c.ClothId == model.id);
            if (cloth == null)
                return null;
            if (model.company != null)
                cloth.Company = DB.Companies.FirstOrDefault(c => c.CompanyId == model.company.id);
            else
                model.company = new CompanyModel { id = cloth.Company.CompanyId, name = cloth.Company.ComanyName };
            if (model.category != null)
                cloth.Category = DB.Categories.FirstOrDefault(c => c.CategoryId == model.category.id);
            else
                model.category = new CategoryModel { id = cloth.Category.CategoryId, name = cloth.Category.CatogoryName };
            if (model.type != null)
                cloth.Type = DB.Types.FirstOrDefault(k => k.TypeId == model.type.id);
            else
                model.type = new TypeModel { id = cloth.Type.TypeId, name = cloth.Type.TypeName };
            if (model.price != 0)
                cloth.Price = model.price;
            else
                model.price = cloth.Price;
            if (model.discount != null)
                cloth.DisCount = (float)(model.discount);
            else
                model.discount = cloth.DisCount;
            if (model.image != null)
                cloth.Image = model.image;
            else
                model.image = cloth.Image;
            DB.SaveChanges();
            return model;
        }

      

        public void DeleteCloth(int id) {
            Cloth cloth = new Cloth { ClothId = id };
            DB.Clothes.Attach(cloth);
            DB.Clothes.Remove(cloth);
            DB.SaveChanges();
        }

        public void UpdatePriceCloth(int id, decimal price) {
            Cloth cloth = DB.Clothes.FirstOrDefault(c => c.ClothId == id);
            cloth.Price = price;
            DB.SaveChanges();
        }
    }
}


