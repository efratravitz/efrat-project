﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class CompanyLogic : BaseLogic {

        public List<CompanyModel> GetAllCompany() {
            return DB.Companies.Select(j => new CompanyModel {
                id = j.CompanyId,
                name = j.ComanyName
            }).ToList();
        }
    }
}
