﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class TypeLogic : BaseLogic {

        public List<TypeModel> GetAllTypes() {
            return DB.Types.Select(j => new TypeModel {
                id = j.TypeId,
                name = j.TypeName
            }).ToList();
        }
       public List<TypeModel>GetTypeByCategoryID(int categoryID) {

         return   DB.Categories.Where(c => c.CategoryId == categoryID)
                .FirstOrDefault().Types
                .Select(t => new TypeModel { id = t.TypeId, name = t.TypeName }).ToList();
               
                
        }
    }
}
