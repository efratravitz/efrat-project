﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class AdminApiController : ApiController {
        private AdminLogic adminLogic = new AdminLogic();

        [HttpPost]
        [Route("api/admin")]
        public HttpResponseMessage CheckLogin([FromBody]AdminModel admin) {
           
            try {
                if (!ModelState.IsValid) {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetAllErrors());
                }
                bool login = adminLogic.CheckAdmin(admin);

                return Request.CreateResponse(HttpStatusCode.OK, login);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());

            }
        }
        protected override void Dispose(bool disposing) {
            if (disposing) {
                adminLogic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
