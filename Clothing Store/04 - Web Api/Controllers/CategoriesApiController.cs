﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class CategoriesApiController : ApiController {
        private CategoriesLogic categoriesgLogic = new CategoriesLogic();
        [HttpGet]
        [Route("api/categories")]
        public HttpResponseMessage GetCategory() {
            try {
                List<CategoryModel> categories = categoriesgLogic.GetAllCategories();
                return Request.CreateResponse(HttpStatusCode.OK, categories);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        protected override void Dispose(bool disposing) {
            if (disposing) {
                categoriesgLogic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
