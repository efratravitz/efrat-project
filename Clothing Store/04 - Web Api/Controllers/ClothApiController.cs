﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    //sites,headers,verbs‏
    [EnableCors("*", "*", "*")]
    public class ClothApiController : ApiController {

        private ClothingLogic clothingLogic = new ClothingLogic();

        [HttpGet]
        [Route("api/clothing")]
        public HttpResponseMessage Getclothes() {
            try {
                List<ClothingModel> clothes = clothingLogic.GetAllClothes();
                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        [HttpGet]
        [Route("api/clothing/{categoryID}")]
        public HttpResponseMessage GetClothesByCategory([FromUri]int categoryID) {

            try {
                List<ClothingModel> clothes = clothingLogic.GetClothesByCategory(categoryID);
                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }

            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        [HttpPost]
        [Route("api/clothing/upload")]

        public HttpResponseMessage UploadImage() {
            try {
                int clothingID = int.Parse(HttpContext.Current.Request.Form["clothingID"]);

                if (HttpContext.Current.Request.Files.Count > 0) {
                    string originalName = HttpContext.Current.Request.Files[0].FileName;
                    string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(originalName);
                    ClothingModel cloth = clothingLogic.UpdatePartialCloth(new ClothingModel { image = newFileName, id = clothingID });
                    string fullPathAndFileName = HttpContext.Current.Server.MapPath("~/Images/" + newFileName);
                    HttpContext.Current.Request.Files[0].SaveAs(fullPathAndFileName);
                    return Request.CreateResponse(HttpStatusCode.OK, cloth);
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { });
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        [HttpGet]
        [Route("api/clothes/{id}")]
        public HttpResponseMessage GetCloth(int id) {
            try {
                ClothingModel cloth = clothingLogic.GetOneCloth(id);
                return Request.CreateResponse(HttpStatusCode.OK, cloth);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        [HttpPost]
        [Route("api/clothing")]
        public HttpResponseMessage AddCloth(ClothingModel clothing) {
            try {
                clothing = clothingLogic.AddCloth(clothing);
                return Request.CreateResponse(HttpStatusCode.Created, clothing);

            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

            }
        }


        [HttpPut]
        [Route("api/clothing/{id}")]
        public HttpResponseMessage UpdateCloth([FromUri]int id) {
            try {
                int categoryId = int.Parse(HttpContext.Current.Request.Form["categoryIdEdit"]);
                string categoryName = HttpContext.Current.Request.Form["categoryNameEdit"];
                int companyId = int.Parse(HttpContext.Current.Request.Form["companyIdEdit"]);
                string companyName = HttpContext.Current.Request.Form["companyNameEdit"];
                int typeId = int.Parse(HttpContext.Current.Request.Form["typeIdEdit"]);
                string typeName = HttpContext.Current.Request.Form["typeNameEdit"];
                decimal price = Convert.ToDecimal(HttpContext.Current.Request.Form["priceEdit"]);
                float discount = Convert.ToSingle(HttpContext.Current.Request.Form["discountEdit"]);
                if (HttpContext.Current.Request.Files.Count > 0) {
                    string orginalName = HttpContext.Current.Request.Files[0].FileName;
                    string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(orginalName);
                    string fullPath = HttpContext.Current.Server.MapPath("~/Images" + newFileName);
                    HttpContext.Current.Request.Files[0].SaveAs(fullPath);
                    ClothingModel cloth = clothingLogic.UpdateFullClothes(new ClothingModel {
                        id = id,
                        category = new CategoryModel { id = categoryId, name = categoryName },
                        company = new CompanyModel { id = id, name = companyName },
                        type = new TypeModel { id = id, name = typeName },
                        price = price,
                        discount = discount,
                        image = newFileName
                    });
                    return Request.CreateResponse(HttpStatusCode.OK, cloth);
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { });
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPatch]
        [Route("api/clothing/{id}")]
        public HttpResponseMessage UpdatePartOfCloth([FromUri]int id, [FromBody]ClothingModel cloth) {
            try {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetAllErrors());
                cloth.id = id;
                cloth = clothingLogic.UpdatePartialCloth(cloth);
                return Request.CreateResponse(HttpStatusCode.Created, cloth);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPut]
        [Route("api/clothing/price/{id}")]
        public HttpResponseMessage UpdatePriceCloth([FromUri]int id, [FromBody]decimal price) {
            try {
                clothingLogic.UpdatePriceCloth(id,price);
                return Request.CreateResponse(HttpStatusCode.Created, true);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpDelete]
        [Route("api/clothing/{id}")]
        public HttpResponseMessage DeleteCloth([FromUri]int id) {
            try {
                File.Delete(HttpContext.Current.Server.MapPath("~/Images/" + clothingLogic.GetOneCloth(id).image));
                clothingLogic.DeleteCloth(id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                clothingLogic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
