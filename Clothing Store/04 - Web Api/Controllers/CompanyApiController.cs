﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]

    public class CompanyApiController : ApiController {
        private CompanyLogic companiesLogic = new CompanyLogic();

        [HttpGet]
        [Route("api/companies")]
        public HttpResponseMessage GetCompany() {
            try {
                List<CompanyModel> companies = companiesLogic.GetAllCompany();
                return Request.CreateResponse(HttpStatusCode.OK, companies);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        protected override void Dispose(bool disposing) {
            if (disposing) {
                companiesLogic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
