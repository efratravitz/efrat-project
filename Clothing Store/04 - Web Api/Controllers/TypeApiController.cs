﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]

    public class TypeApiController : ApiController {
        private TypeLogic typeLogic = new TypeLogic();

        [HttpGet]
        [Route("api/types")]
        public HttpResponseMessage GetTypes() {
            try {
                List<TypeModel> types = typeLogic.GetAllTypes();
                return Request.CreateResponse(HttpStatusCode.OK, types);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpGet]
        [Route("api/types/category/{id}")]
        public HttpResponseMessage GetAllCategories([FromUri]int id) {
            try {
                return Request.CreateResponse(HttpStatusCode.OK, typeLogic.GetTypeByCategoryID(id));
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }
        protected override void Dispose(bool disposing) {
            if (disposing) {
                typeLogic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

