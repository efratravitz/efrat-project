﻿$(function () {
    $.ajax({
        method: "GET",
        url: "http://localhost:53207/api/categories",
        cache: false,
        error: function (err) {
            alert(err.status + ", " + err.statusText);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#categoryEdit").append(option);
            }
            document.getElementById("categoryEdit").selectedIndex = -1;
        }
    });

    $.ajax({
        method: "GET",
        url: "http://localhost:53207/api/companies",
        cache: false,
        error: function (err) {
            var errors = err.responseJSON;
            var error = "";
            for (var item in errors) {
                for (var i = 0; i < errors[item].length; i++) {
                    error += errors[item][i];

                }
            }
            alert("Error: " + error);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#companyEdit").append(option);
            }
            document.getElementById("companyEdit").selectedIndex = -1;
        }
    });

    $.ajax({
        method: "GET",
        url: "http://localhost:53207/api/types",
        cache: false,
        error: function (err) {
            var errors = err.responseJSON;
         
            for (var item in errors) {
                for (var i = 0; i < errors[item].length; i++) {
                    error += errors[item][i];
              
                }
            }
            alert("Error: " + error);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#typeEdit").append(option);
            }
            document.getElementById("typeEdit").selectedIndex = -1;
        }
    });

    $("#editCloth").click(function () {
        
        alert("added");
        if ($('#priceEdit').val() == '')
            alert("Missing Price");
        if ($('#discountEdit').val() < 0 || $('#discountEdit').val() > 1000 && $('#discountEdit').val()!="")
            alert("Discont can't be negative and can't be large than 100");
    


        var formData = new FormData();
        formData.append("categoryIdEdit", $("#categoryEdit option:selected").val());
        formData.append("categoryNameEdit", $("#categoryEdit option:selected").text());
        formData.append("companyIdEdit", $("#companyEdit option:selected").val());
        formData.append("companyNameEdit", $("#companyEdit option:selected").text());
        formData.append("typeIdEdit", $("#typeEdit option:selected").val());
        formData.append("typeNameEdit", $("#typeEdit option:selected").text());
        formData.append("priceEdit", $('#priceEdit').val());
        formData.append("discountEdit", $('#discountEdit').val());
        var files = document.getElementById("imageEdit").files;
        if (files.length > 0) {
            formData.append("imageEdit", files[0]);
        }
        if ($("#dialogEdit").data('ifUpdate') == null) {
            $.ajax({
                type: "POST",
                url: "http://localhost:53207/api/clothing",
                data: formData,
                error: function (err) {
                    alert(err.status + ", " + err.statusText);
                },
                success: function (response) {
                    if (response.discount > 0) {
                        var newprice = parseInt(response.price - response.discount * response.price / 100);
                        var price = '<p class="price"><del>' + response.price + '</del></p>' + '<p class="newprice" style="color:red">' + newprice + '</p>';
                    }
                    else

                        var price = '<p class="price">' + response.price + '</p>';
                    var item = '<div class= ' + response.id + '>' + '<p class="category">' + response.category.name + '</p>' + '<p class="company">' + response.company.name + '</p>' + '<p class="category">' + response.type.name + '</p>' + price + "<img src='http://localhost:53207/Images/" + response.image + "'/>" + '</div>';
                    $("#editedCloth").html(item);
                    $("#typeEdit").val(1);
                    $("#companyEdit").val(1);
                    $("#categoryEdit").val(1);
                    $("#discountEdit").val("");
                    $("#priceEdit").val("");
                    $("#editedCloth").empty();
                    $("#dialogEdit").data('ifUpdate', null);
                },
                contentType: false,
                processData: false
            });
        }
        else {
            var id = $("#editCloth").attr('class');
            $.ajax({
                type: "PUT",
                url: "http://localhost:53207/api/clothing/" + id,
                data: formData,
                error: function (err) {
                    var errors = err.responseJSON;
                   
                    for (var item in errors) {
                        for (var i = 0; i < errors[item].length; i++) {
                            error += errors[item][i];
                          
                        }
                    }
                    alert("Error: " + error);
                },
                success: function (response) {
                    if (response.discount > 0) {
                        var newprice = parseInt(response.price - response.discount * response.price / 100);
                        var price = '<p class="price"><del>' + response.price + '</del></p>' + '<p class="newprice" style="color:red">' + newprice + '</p>';
                    }
                    else

                        var price = '<p class="price">' + response.price + '</p>';
                    var item = '<div class= ' + response.id + '>' + '<p class="category">' + response.category.name + '</p>' + '<p class="company">' + response.company.name + '</p>' + '<p class="category">' + response.type.name + '</p>' + price + "<img src='http://localhost:53207/Images/" + response.image + "'/>" + '</div>';
                    $("#editedCloth").html(item);
                    $("#typeEdit").val(1);
                    $("#companyEdit").val(1);
                    $("#categoryEdit").val(1);
                    $("#discountEdit").val("");
                    $("#priceEdit").val("");
                    $("#editedCloth").empty();
                    $("#dialogEdit").data('ifUpdate', null);
                },
                contentType: false,
                processData: false
            });
        }
        //location.reload();
    });
});