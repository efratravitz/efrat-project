function makeAjaxRequest(url,callback)
{
	if (window.XMLHttpRequest)
	{
		var http_request = new XMLHttpRequest();
	}
	else
	{
		if (window.ActiveXObject)
		{
			var http_request = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	http_request.onreadystatechange = function()
	{
		if (http_request.readyState == 4)
		{
			if (http_request.responseXML != null)
			{
				eval(callback.replace("XML_OBJECT",http_request.responseXML));
			}
		}
	}
	http_request.open("GET",url,true);
	http_request.send("");
}

function makeTextRequest(url,callback)
{
	if (window.XMLHttpRequest)
	{
		var http_request = new XMLHttpRequest();
	}
	else
	{
		if (window.ActiveXObject)
		{
			var http_request = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	http_request.onreadystatechange = function()
	{
		if (http_request.readyState == 4)
		{
			eval(callback.replace("XML_OBJECT",http_request.responseText));
		}
	}
	http_request.open("GET",url,true);
	http_request.send("");
}

function getNode(obj,pos)
{
	if (window.XMLHttpRequest)
		return obj[pos];
	else
		return obj.item(pos);
}

function getSingleNode(obj,tagName)
{
	if (window.XMLHttpRequest)
		return getRawNode(obj,tagName).selectSingleNode("text()").nodeValue;
	else
		return getRawNode(obj,tagName).text;
}

function getRawNode(obj,tagName)
{
	return obj.selectSingleNode(tagName);
}

function getAttribute(obj,attName)
{
	if (window.XMLHttpRequest)
		return obj.selectSingleNode("@" + attName).nodeValue;
	else
		return obj.getAttribute(attName);
}