﻿$(function () {
    // Get the modal
    var modal = document.getElementById('id01');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }




    if (sessionStorage.getItem('Admin') == 'true') {
        var add = "<input type='button' value='AddCloth' class='addCloth'/>";
        $('#add').html(add);
        var button = "<input type='button' value='logout' class='logout'/>";
        $("#logout").html(button);

    }

    $.ajax({
        method: "GET",
        url: "http://localhost:53207/api/categories",
        cache: false,
        error: function (err) {
            alert(err.status + ", " + err.statusText);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option dir=rtl value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#select").append(option);
            }
            document.getElementById("select").selectedIndex = -1;
        }
    });
    $("#add").on('click', '.addCloth', function () {
      
        $("#dialogEdit").dialog();
    });

    $("#logout").on('click', '.logout', function () {
        sessionStorage.removeItem('Admin');
        window.open("index.html");
    });

    $("#login").prop('disabled', true);
    $('#password,#name').keyup(function () {
        if ($("#password").val() != '' && $("#name").val() != '') {
            $("#login").prop('disabled', false);
        }
        else {
            $("#login").prop('disabled', true);
        }
    });

    $("#select").change(function () {
        $.ajax({
            method: "GET",
            url: "http://localhost:53207/api/categories/" + $(this).val(),
            cache: false,
            error: function (err) {
                alert(err.status + ", " + err.statusText);
            },
            success: function (response) {
                $("#container").empty();

                for (var i = 0; i < response.length; i++) {
                    var item = "<div id='image'><img class='image' src='http://localhost:53207/Images/" + response[i].image + "' id='" + response[i].id + "'/>";
                    if (sessionStorage.getItem('Admin') == 'true') {
                        item += "<input type='button' value='Delete' id='" + response[i].id + "' class='delete''/><input type='button' value='Update' id='" + response[i].id + "' class='update'/>";
                    }
                    item += "</div>";
                    $("#container").append(item);
                }
            }
        });
    });

    $("#container").on('click', '.delete', function () {
        $.ajax({
            method: "DELETE",
            url: "http://localhost:53207/api/clothing/" + $(this).attr('id'),
            chace: false,
            error: function (err) {
                alert(err.status + ", " + err.statusText);
            },
            success: function (response) {
                alert("deleted");
                location.reload();
                
            }
        });
    });

    $("#container").on('click', '.update', function () {
        var id = $(this).attr('id');
        showUpdateDetailes(id);


    });

    function showUpdateDetailes(id) {
       
        $("#dialogEdit").data('ifUpdate', "update");
        $.ajax({
            method: "GET",
            url: "http://localhost:53207/api/clothing/" + id,
            chace: false,
            error: function (err) {
                alert(err.status + "," + err.statusText);
            },
            success: function (response) {
                $("#typeEdit").val(response.type.id);
                $("#companyEdit").val(response.company.id);
                $("#categoryEdit").val(response.category.id);
                $("#discountEdit").val(response.discount);
                $("#priceEdit").val(response.price);
                $("#editCloth").removeClass();
                $("#editCloth").addClass(id);
                $("#dialogEdit").dialog();
            }
        });
    }

    $("#container").on('click', '.image', function () {
        $.ajax({
            type: "GET",
            url: "http://localhost:53207/api/clothing/" + $(this).attr('id'),
            cache: false,
            error: function (err) {
                alert(err.status + ", " + err.statusText);
            },
            success: function (response) {
                if (response.discount > 0) {
                    var newprice = parseInt(response.price - response.discount * response.price / 100);
                    var price = '<p class="price"><del>' + response.price + '</del></p>' + '<p class="newprice" style="color:white">' + newprice + '</p>';
                }
                else
                    var price = '<p class="price">' + response.price + '</p>';
                var item = '<div class= ' + response.id + '>' + '<p class="category">' + response.category.name + '</p>' + '<p class="company">' + response.company.name + '</p>' + '<p class="category">' + response.type.name + '</p>' + price + "<img src='http://localhost:53207/Images/" + response.image + "'/>" + '</div>';
                $("#dialogDetailes").html(item);
                $("#dialogDetailes").dialog();
            }
        });
    });

    $("#login").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost:53207/api/admin",
            data: {
                name: $("#name").val(),
                password: $("#password").val()
            },
            cache: false,
            error: function (err) 
            {


                var errors = err.responseJSON;
                //var error = "";
                for (var item in errors) {
                    for (var i = 0; i < errors[item].length; i++) {
                        error += errors[item][i];
                        error += "/n";
                    }
                }
                alert("Error: " + error);
            },
            success: function (response) {
                sessionStorage.setItem('Admin', response);
                if (sessionStorage.getItem('Admin') == 'true') {
                    {
                        window.open("index.html");
                    }
                }
            }
        });
    });
});






