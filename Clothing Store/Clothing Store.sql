USE [master]
GO
/****** Object:  Database [Clothing Store]    Script Date: 31/08/2017 09:17:05 ******/
CREATE DATABASE [Clothing Store]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Clothing Store', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Clothing Store.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Clothing Store_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Clothing Store_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Clothing Store] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Clothing Store].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Clothing Store] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Clothing Store] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Clothing Store] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Clothing Store] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Clothing Store] SET ARITHABORT OFF 
GO
ALTER DATABASE [Clothing Store] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Clothing Store] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Clothing Store] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Clothing Store] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Clothing Store] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Clothing Store] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Clothing Store] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Clothing Store] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Clothing Store] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Clothing Store] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Clothing Store] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Clothing Store] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Clothing Store] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Clothing Store] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Clothing Store] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Clothing Store] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Clothing Store] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Clothing Store] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Clothing Store] SET  MULTI_USER 
GO
ALTER DATABASE [Clothing Store] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Clothing Store] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Clothing Store] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Clothing Store] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Clothing Store] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Clothing Store]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 31/08/2017 09:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminId] [int] IDENTITY(1,1) NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
	[AdminPassword] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 31/08/2017 09:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CatogoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clothes]    Script Date: 31/08/2017 09:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clothes](
	[ClothId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[ComanyId] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[DisCount] [real] NULL,
	[Image] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Clothes] PRIMARY KEY CLUSTERED 
(
	[ClothId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Companies]    Script Date: 31/08/2017 09:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[ComanyName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Types]    Script Date: 31/08/2017 09:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Types] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([AdminId], [AdminName], [AdminPassword]) VALUES (1, N'sara', N'12345')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryId], [CatogoryName]) VALUES (1, N'נשים')
INSERT [dbo].[Categories] ([CategoryId], [CatogoryName]) VALUES (2, N'גברים')
INSERT [dbo].[Categories] ([CategoryId], [CatogoryName]) VALUES (3, N'ילדים')
INSERT [dbo].[Categories] ([CategoryId], [CatogoryName]) VALUES (4, N'תינוקות')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Clothes] ON 

INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (23, 1, 1, 1, 123.0000, 3, N'WomenDress.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (25, 1, 2, 4, 454.0000, 1, N'Dress3.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (26, 3, 2, 1, 125.0000, 23, N'tshirt.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (27, 3, 2, 4, 213.0000, 4, N'sweaterchildren.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (29, 3, 2, 3, 245.0000, 23, N'blouse1.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (31, 3, 2, 1, 153.0000, 34, N'sweater.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (33, 3, 2, 3, 78.0000, 355, N'7de77fcf-1483-4d66-a0c2-9a49b8348f11.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (34, 3, 2, 2, 61.0000, 12, N'0069289_0.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (35, 1, 1, 1, 44.0000, 5, N'e3e088df-d1e6-4d99-919d-b08b4ba02bb8.jpeg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [TypeId], [ComanyId], [Price], [DisCount], [Image]) VALUES (36, 1, 3, 2, 54.0000, 5432, N'08d0ae2a-ff47-4271-82f1-e4d2cbef4c91.jpg')
SET IDENTITY_INSERT [dbo].[Clothes] OFF
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([CompanyId], [ComanyName]) VALUES (1, N'Zara')
INSERT [dbo].[Companies] ([CompanyId], [ComanyName]) VALUES (2, N'Golf')
INSERT [dbo].[Companies] ([CompanyId], [ComanyName]) VALUES (3, N'Castro')
INSERT [dbo].[Companies] ([CompanyId], [ComanyName]) VALUES (4, N'Solog')
SET IDENTITY_INSERT [dbo].[Companies] OFF
SET IDENTITY_INSERT [dbo].[Types] ON 

INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (1, N'שמלה ')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (2, N'חולצה')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (3, N'חצאית')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (4, N'סריג')
SET IDENTITY_INSERT [dbo].[Types] OFF
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Categories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Categories]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Companies] FOREIGN KEY([ComanyId])
REFERENCES [dbo].[Companies] ([CompanyId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Companies]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Types] FOREIGN KEY([TypeId])
REFERENCES [dbo].[Types] ([TypeId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Types]
GO
USE [master]
GO
ALTER DATABASE [Clothing Store] SET  READ_WRITE 
GO
